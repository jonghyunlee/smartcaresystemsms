package com.smartcaresystemsms.http;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;

import android.content.Context;

public class AsyncFileDownloader {

	private Context context;

	public AsyncFileDownloader(Context context) {
		this.context = context;
	}

	public void download(String url, String savePath, AsyncCallback<File> callback) {
		download(url, new File(savePath), callback);
	}
	
	public void download(String url, String savePath, AsyncCallback<File> callback, IProgressNotify pb) {
		download(url, new File(savePath), callback,pb);
	}

	public void download(String url, File destination, AsyncCallback<File> callback) {
		try {
			destination = getDestinationIfNotNullOrCreateTemp(destination, callback);
		} catch (IOException e) {
			callback.exceptionOccured(e);
			return;
		}
		runAsyncDownload(url, destination, callback);
	}
	
	public void download(String url, File destination, AsyncCallback<File> callback, IProgressNotify pb) {
		try {
			destination = getDestinationIfNotNullOrCreateTemp(destination, callback);
		} catch (IOException e) {
			callback.exceptionOccured(e);
			return;
		}
		runAsyncDownload(url, destination, callback, pb);
	}

	private File getDestinationIfNotNullOrCreateTemp(File destination, AsyncCallback<File> callback) throws IOException {
		if (destination != null) {
			return destination;
		}
		return createTemporaryFile();
	}

	private File createTemporaryFile() throws IOException {
		return File.createTempFile("afd", ".tmp", context.getCacheDir());
	}

	private void runAsyncDownload(String url, File destination, AsyncCallback<File> callback) {
//		LogMgr.d("APP_UPDATE", "runAsyncDownload....url : " + url);
		Callable<File> callable = new FileDownloadCallable(url, destination);
		new AsyncExecutor<File>().setCallable(callable).setCallback(callback).execute();
	}
	
	private void runAsyncDownload(String url, File destination, AsyncCallback<File> callback, IProgressNotify pb) {
		Callable<File> callable = new FileDownloadCallable(url, destination,pb);
		new AsyncExecutor<File>().setCallable(callable).setCallback(callback).execute();
	}

}