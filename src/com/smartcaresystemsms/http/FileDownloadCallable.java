package com.smartcaresystemsms.http;

import java.io.File;
import java.util.concurrent.Callable;

public class FileDownloadCallable implements Callable<File> {
	private String url;
	private File file;
	private IProgressNotify pb;

	public FileDownloadCallable(String url, File file) {
		this.url = url;
		this.file = file;
	}
	
	public FileDownloadCallable(String url, File file,IProgressNotify pb) {
		this.url = url;
		this.file = file;
		this.pb =pb;
	}

	@Override
	public File call() throws Exception {
		return HttpRequestHelper.getInstance().download(url, file,pb);
	}

}
