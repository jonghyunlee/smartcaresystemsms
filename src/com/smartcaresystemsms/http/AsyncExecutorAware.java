package com.smartcaresystemsms.http;

public interface AsyncExecutorAware<T> {

	public void setAsyncExecutor(AsyncExecutor<T> asyncExecutor);

}