package com.smartcaresystemsms.http;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

public enum HttpUtils {
	INSTANCE;
	
	final public HttpResponse httpUrlPOSTRequest(final String upload_url,final HttpEntity entity) throws ClientProtocolException, IOException {
		
		final HttpClient httpclient = new DefaultHttpClient();
		final HttpPost httpost = new HttpPost(upload_url);
        httpost.setEntity(entity);
        
        return httpclient.execute(httpost);
	}
	
	final public HttpResponse httpUrlGETRequest(final String request_url) throws ClientProtocolException, IOException {
		final HttpClient httpclient = new DefaultHttpClient();
		final HttpGet httpget = new HttpGet(request_url);
		httpget.setHeader("Accept-Charset", "UTF-8");
		
		return httpclient.execute(httpget);

	}	
}
