package com.smartcaresystemsms.http;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

import android.net.http.AndroidHttpClient;


public class HttpRequestHelper {

	public File download(String url, File toFile) throws IOException {
		AndroidHttpClient client = AndroidHttpClient.newInstance("ANDROID");
		HttpGet getRequest = new HttpGet(url);
		try {
			HttpResponse response = client.execute(getRequest);
			checkStatusAndThrowExceptionWhenStatusIsNotOK(response);
			return writeResponseToFileAndGet(response.getEntity(), toFile);
		} catch (IOException e) {
			getRequest.abort();
			throw e;
		} finally {
			client.close();
		}
	}
	
	public File download(String url, File toFile,IProgressNotify pb) throws IOException {
		AndroidHttpClient client = AndroidHttpClient.newInstance("ANDROID");
		HttpGet getRequest = new HttpGet(url);
		try {
			HttpResponse response = client.execute(getRequest);
			checkStatusAndThrowExceptionWhenStatusIsNotOK(response);
			return writeResponseToFileAndGet(response.getEntity(), toFile,pb);
		} catch (IOException e) {
			getRequest.abort();
			throw e;
		} finally {
			client.close();
		}
	}

	private void checkStatusAndThrowExceptionWhenStatusIsNotOK(HttpResponse response) throws IOException {
		int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode != HttpStatus.SC_OK) {
			throw new IOException("invalid response code:" + statusCode);
		}
	}

	private File writeResponseToFileAndGet(HttpEntity entity, File toFile) throws IOException {
		InputStream in = null;
		try {
			IOUtils.copy(entity.getContent(), toFile);
			return toFile;
		} finally {
			IOUtils.close(in);
			entity.consumeContent();
		}
	}
	
	private File writeResponseToFileAndGet(HttpEntity entity, File toFile,IProgressNotify pb) throws IOException {
		InputStream in = null;
		try {
			if( pb == null ) IOUtils.copy(entity.getContent(), toFile);			
			else IOUtils.copy(entity.getContent(), toFile,pb);
			return toFile;
		} finally {
			IOUtils.close(in);
			entity.consumeContent();
		}
	}
	
	
	class HttpUrlRequestCallable implements Callable<HttpResponse>
	{
		String url;
		
		HttpUrlRequestCallable(String url){
			this.url = url;
		}

		@Override
		public HttpResponse call() throws Exception {
			AndroidHttpClient client = AndroidHttpClient.newInstance("ANDROID");
			HttpGet getRequest = new HttpGet(url);
			HttpResponse response = null;
			
			try {
				response = client.execute(getRequest);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();	
			}finally {
				client.close();
			}
			
			return response;
		}
		
	}
	
	public String urlrequest(String url) 
	{
		String content="";
		HttpResponse response = null;
		ExecutorService executor = Executors.newFixedThreadPool(1);
		Future<HttpResponse> f = executor.submit(new HttpUrlRequestCallable(url));
		
		try {
			response = f.get();
			HttpEntity entity = response.getEntity();
			content = EntityUtils.toString(entity);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		executor.shutdown();
		
		return content;		
	}
	

	public static HttpRequestHelper getInstance() {
		return new HttpRequestHelper();
	}

}