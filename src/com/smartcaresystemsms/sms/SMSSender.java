package com.smartcaresystemsms.sms;

import java.util.ArrayList;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
/**
 * @author Jong
 *
 */
public class SMSSender {
	Context context;
	public SMSSender(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	public void sendSMS(String phoneNumber, String smsMessage){
		PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0,new Intent("SMS_SENT_ACTION"), 0);
		PendingIntent deliveredIntent = PendingIntent.getBroadcast(context, 0,new Intent("SMS_DELIVERED_ACTION"), 0);
		if(phoneNumber == null || phoneNumber.equals("")){
			return;
		}
		context.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					break;
				}
			}
		}, new IntentFilter("SMS_SENT_ACTION"));

		context.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					break;
				case Activity.RESULT_CANCELED:
					break;
				}
			}
		}, new IntentFilter("SMS_DELIVERED_ACTION"));

		SmsManager mSmsManager = SmsManager.getDefault();
		ArrayList<String> parseMessage = mSmsManager.divideMessage(smsMessage);
		mSmsManager.sendMultipartTextMessage(phoneNumber, null, parseMessage, null, null);
	}
}
