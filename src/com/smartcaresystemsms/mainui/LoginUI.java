package com.smartcaresystemsms.mainui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartcaresystemsms.R;

public class LoginUI extends Activity implements OnClickListener{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		Button loginButton = (Button)findViewById(R.id.loginButton);
		TextView enrollmentTextview = (TextView) findViewById(R.id.enrollmentTextview);
		enrollmentTextview.setOnClickListener(this);
		loginButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.loginButton:
			intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			Toast.makeText(this, "시작 클릭", Toast.LENGTH_SHORT).show();	
		break;
		case R.id.enrollmentTextview:
//			try {
//				Aes128 aes = Aes128.INSTANCE;
//				aes.init();
//				String sendData = "UCF:0,,119,,01051786548,01024578421,1234#";
//				sendData = Aes128.byteArrayToHexNoSpace(aes.encrypt(sendData.getBytes()), aes.encrypt(sendData.getBytes()).length);
//				Toast.makeText(this, sendData, Toast.LENGTH_SHORT).show();
//				SMSSender smsSender = new SMSSender(getApplicationContext());
//				smsSender.sendSMS("01221219763", sendData);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			intent = new Intent(this, EnrollmentUI.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			Toast.makeText(this, "회원가입 클릭", Toast.LENGTH_SHORT).show();
		break;

		default:
			break;
		}
	}

}
