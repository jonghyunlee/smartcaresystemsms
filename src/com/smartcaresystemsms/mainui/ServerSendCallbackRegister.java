package com.smartcaresystemsms.mainui;

public class ServerSendCallbackRegister {
	private ServerSendCallback serverSendCallback;
	
	public ServerSendCallbackRegister(ServerSendCallback serverSendCallback) {
		this.serverSendCallback = serverSendCallback;
	}
	
	public void execute(String sendData){
		serverSendCallback.serverSendMethod(sendData);
	}
}
