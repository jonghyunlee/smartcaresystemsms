package com.smartcaresystemsms.mainui;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.protocol.HTTP;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.smartcaresystemsms.R;
import com.smartcaresystemsms.Constant;
import com.smartcaresystemsms.http.HttpUtils;

public class EnrollmentUI extends Activity implements OnClickListener {
	private final static String TAG = "EnrollmentUI";
	static Context context;
	EditText userNameEditText, emailEditText, centerNameEditText,
			centerNumEditText;
	Spinner areaSpinner_01, areaSpinner_02;
	Button sendButton, cancelButton;
	String[] mainareas;
	String mainarea;
	String[] subareas;
	String subarea;
	
	static String user_id;
	static String name;
	static String user_email;
	static String user_area;
	static String crc_name;
	static String crc_num;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enrollment);
		
		context = this;

		userNameEditText = (EditText) findViewById(R.id.userNameEditText);
		emailEditText = (EditText) findViewById(R.id.emailEditText);
		areaSpinner_01 = (Spinner) findViewById(R.id.areaSpinner_01);
		areaSpinner_02 = (Spinner) findViewById(R.id.areaSpinner_02);
		centerNameEditText = (EditText) findViewById(R.id.centerNameEditText);
		centerNumEditText = (EditText) findViewById(R.id.centerNumEditText);

		sendButton = (Button) findViewById(R.id.sendButton);
		cancelButton = (Button) findViewById(R.id.cancelButton);
		
		mainareas = getResources().getStringArray(R.array.mainarea);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, mainareas);
		
		areaSpinner_01.setPrompt("시 도");
		areaSpinner_01.setAdapter(adapter);
		areaSpinner_01.setOnItemSelectedListener(MainonItemSelectedListener);
		
		sendButton.setOnClickListener(this);
		cancelButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		user_id = getMyPhoneNumber();
		name = userNameEditText.getText().toString();
		user_email = emailEditText.getText().toString();
		user_area = mainarea + " " + subarea;
		crc_name = centerNameEditText.getText().toString();
		crc_num = centerNumEditText.getText().toString();
		
		Log.d(TAG, "name : " + name + "...user_email : " + user_email + "...user_area : " + user_area + "...crc_name : " + crc_name + "...crc_num : " + crc_num);
		switch (v.getId()) {
		case R.id.sendButton:
//			asdasdasdasd
			try {
				boolean result = new SendEnrollment().execute().get();
				if(result){
					Toast.makeText(getApplicationContext(), "정상적으로 회원가입 요청을 하였습니다.", Toast.LENGTH_SHORT).show();
					finish();
				}
				else
					Toast.makeText(getApplicationContext(), "회원가입 요청 중 오류가 발생하였습니다.\n해당 오류가 계속된다면 관리자에게 문의하세요.", Toast.LENGTH_SHORT).show();
			} catch (InterruptedException e) {
				Toast.makeText(getApplicationContext(), "회원가입 요청 중 오류가 발생하였습니다.\n해당 오류가 계속된다면 관리자에게 문의하세요.", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			} catch (ExecutionException e) {
				Toast.makeText(getApplicationContext(), "회원가입 요청 중 오류가 발생하였습니다.\n해당 오류가 계속된다면 관리자에게 문의하세요.", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
			break;
		case R.id.cancelButton:
			finish();
			break;

		default:
			break;
		}
		
	}

	private String getMyPhoneNumber() {
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		String number = telephonyManager.getLine1Number();

		if (number != null) {
			if (number.startsWith("+82")) {
				number = number.replace("+82", "0");
			}
		} else {

		}

		return number;
	}
	OnItemSelectedListener MainonItemSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
			mainarea = mainareas[pos];
			
			switch (pos) {
			case Constant.GANGWONDO_INDEX:
				subareas = getResources().getStringArray(R.array.gangwondoarea);
				break;
				
			case Constant.GYEONGGIDO_INDEX:
				subareas = getResources().getStringArray(R.array.gyeonggidoarea);
				break;
				
			case Constant.GYEONGSANGNAMDO_INDEX:
				subareas = getResources().getStringArray(R.array.gyeongsangnamdoarea);
				break;
				
			case Constant.GYEONGSANGBUKDO_INDEX:
				subareas = getResources().getStringArray(R.array.gyeongsangbukdoarea);
				break;
				
			case Constant.GWANGJU_INDEX:
				subareas = getResources().getStringArray(R.array.gwangjuarea);
				break;
				
			case Constant.DAEGU_INDEX:
				subareas = getResources().getStringArray(R.array.daeguarea);
				break;
				
			case Constant.DAEJEON_INDEX:
				subareas = getResources().getStringArray(R.array.daejeonarea);
				break;
				
			case Constant.BUSAN_INDEX:
				subareas = getResources().getStringArray(R.array.busanarea);
				break;
				
			case Constant.SEOUL_INDEX:
				subareas = getResources().getStringArray(R.array.seoularea);
				break;
				
			case Constant.SEJONG_INDEX:
				subareas = getResources().getStringArray(R.array.sejongarea);
				break;
				
			case Constant.ULSAN_INDEX:
				subareas = getResources().getStringArray(R.array.ulsanarea);
				break;
				
			case Constant.INCHEON_INDEX:
				subareas = getResources().getStringArray(R.array.incheonarea);
				break;
				
			case Constant.JEOLLANAMDO_INDEX:
				subareas = getResources().getStringArray(R.array.jeollanamdoarea);
				break;
				
			case Constant.JEOLLABUKDO_INDEX:
				subareas = getResources().getStringArray(R.array.jeollabukdoarea);
				break;
				
			case Constant.JEJU_INDEX:
				subareas = getResources().getStringArray(R.array.jejuarea);
				break;
				
			case Constant.CHUNGCHEONGNAMDO_INDEX:
				subareas = getResources().getStringArray(R.array.chungcheongnamdoarea);
				break;
				
			case Constant.CHUNGCHEONGBUKDO_INDEX:
				subareas = getResources().getStringArray(R.array.chungcheongbukdoarea);
				break;

			default:
				break;
			}
			
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, subareas);
			areaSpinner_02.setPrompt(mainarea);
			areaSpinner_02.setAdapter(adapter);
			areaSpinner_02.setOnItemSelectedListener(SubonItemSelectedListener);
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			
		}
	};
	
	OnItemSelectedListener SubonItemSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
			// TODO Auto-generated method stub
			subarea = subareas[pos];
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			
		}
	};
	
	private static class SendEnrollment extends AsyncTask<Void, Void, Boolean>{
		
		@SuppressWarnings("deprecation")
		@Override
		protected Boolean doInBackground(Void...voids) {
			boolean result = true;
			Log.d(TAG, "name : " + name + "...user_email : " + user_email + "...user_area : " + user_area + "...crc_name : " + crc_name + "...crc_num : " + crc_num);

			try {
				HttpEntity multipart = MultipartEntityBuilder.create()
						.addTextBody("user_id", user_id)
						.addPart("name",(ContentBody) new StringBody(name, Charset.forName(HTTP.UTF_8)))
						.addTextBody("user_email", user_email)
						.addPart("user_area",(ContentBody) new StringBody(user_area, Charset.forName(HTTP.UTF_8)))
						.addPart("crc_name",(ContentBody) new StringBody(crc_name, Charset.forName(HTTP.UTF_8)))
						.addTextBody("crc_num", crc_num)
						.build();
				HttpUtils.INSTANCE.httpUrlPOSTRequest(Constant.ENROLLMENT_PATH, multipart);
				result = true;
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result = false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result = false;
			}
			return result;
		}

		
	}

}
