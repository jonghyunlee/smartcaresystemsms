package com.smartcaresystemsms.mainui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.example.smartcaresystemsms.R;
import com.smartcaresystemsms.Constant;
import com.smartcaresystemsms.sms.SMSSender;


public class MainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private final static String TAG = "MainActivity";
    final private static String TRANS_ID = "51152#";
    private static String sendData;
    private static int position;
    private static View getrootView;
    private static int systemMode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }
    
    public void setTitle(String title){
    	mTitle = title;
    	
    };

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_example) {  
        	StringBuilder stringBuilder = new StringBuilder();
        	switch(position){
            case Constant.SMS_INFOMATION_INDEX:
        		sendData = "보내실 데이터가 없습니다.";
        		break;
        	
            case Constant.SMS_RESULT_CHECK_INDEX:
            	stringBuilder
        		.append(Constant.SMS_RESULT_CHECK)
        		.append(",").append(TRANS_ID);
            	break;
        	
            case Constant.SMS_HOT_LINE_NUM_INDEX:
            	EditText centerEditText = (EditText) getrootView.findViewById(R.id.centerEditText);
        		EditText friendEditText_01 = (EditText) getrootView.findViewById(R.id.friendEditText_01);
        		EditText friendEditText_02 = (EditText) getrootView.findViewById(R.id.friendEditText_02);
        		EditText careEditText = (EditText) getrootView.findViewById(R.id.careEditText);
        		
        		int errorCode;
        		String nineOneOneNumber = "119";
        		String centerNumber = centerEditText.getText().toString();
        		String friendNumber_01 = friendEditText_01.getText().toString();
        		String friendNumber_02 = friendEditText_02.getText().toString();
        		String careNumber = careEditText.getText().toString();
        		
        		errorCode = validNumber(centerNumber, getResources().getString(R.string.center));
        		if(errorCode != Constant.VALIDE_NUM)
        			return true;
        		
        		errorCode = validNumber(friendNumber_01, getResources().getString(R.string.friend_01));
        		if(errorCode != Constant.VALIDE_NUM)
        			return true;
        		
        		errorCode = validNumber(friendNumber_02, getResources().getString(R.string.friend_02));
        		if(errorCode != Constant.VALIDE_NUM)
        			return true;
        		
        		errorCode = validNumber(careNumber, getResources().getString(R.string.care));
        		if(errorCode != Constant.VALIDE_NUM)
        			return true;
        		
        		stringBuilder
        		.append(Constant.SMS_HOT_LINE_NUM)
        		.append(",").append(centerNumber)
        		.append(",").append(nineOneOneNumber)
        		.append(",").append(friendNumber_01)
        		.append(",").append(friendNumber_02)
        		.append(",").append(careNumber)
        		.append(",").append(TRANS_ID);
            	
            	break;
        	
            case Constant.SMS_REMOTE_OPEN_INDEX:
            	stringBuilder
        		.append(Constant.SMS_REMOTE_OPEN);
            	break;
        	
            case Constant.SMS_SYSTEM_MODE_SETTING_INDEX:
            	if(systemMode == 0){
            		Toast.makeText(getApplicationContext(), "시스템 모드를 선택해 주세요.", Toast.LENGTH_SHORT).show();
            		return true;
            	}
            	
            	stringBuilder
        		.append(Constant.SMS_SYSTEM_MODE_SETTING)
        		.append(",").append(systemMode);
            	break;
        		
            case Constant.SMS_GATEWAY_FIRMWARE_UPDATE_INDEX:
            	stringBuilder
        		.append(Constant.SMS_GATEWAY_FIRMWARE_UPDATE);
            	break;
            	
            case Constant.SMS_ZIGBEE_FIRMWARE_UPDATE_INDEX:
            	stringBuilder
        		.append(Constant.SMS_ZIGBEE_FIRMWARE_UPDATE);
            	break;
            	
            case Constant.SMS_GATEWAY_INFORMATION_INDEX:
            	stringBuilder
        		.append(Constant.SMS_GATEWAY_INFORMATION);
            	break;
            	
            case Constant.SMS_SENSOR_INFORMATION_INDEX:
            	stringBuilder
        		.append(Constant.SMS_SENSOR_INFORMATION);
            	break;
            	
            case Constant.SMS_IN_MODE_SETTING_INDEX:
            	stringBuilder
        		.append(Constant.SMS_IN_MODE_SETTING);
            	break;
            	
            case Constant.SMS_POWER_RECOVERY_INDEX:
            	stringBuilder
        		.append(Constant.SMS_POWER_RECOVERY);
            	break;
            	
            case Constant.SMS_ACTIVITY_INDEX:
            	stringBuilder
        		.append(Constant.SMS_ACTIVITY);
            	break;
        		
            case Constant.SMS_DATA_NOT_RECEIVE_INDEX:
            	stringBuilder
        		.append(Constant.SMS_DATA_NOT_RECEIVE);
            	break;
            	
            case Constant.SMS_NOT_MATCH_AREA_INDEX:
            	stringBuilder
        		.append(Constant.SMS_NOT_MATCH_AREA);
            	break;
            	
            case Constant.SMS_PERIOD_REPORT_INDEX:
            	stringBuilder
        		.append(Constant.SMS_PERIOD_REPORT);
            	break;
            	
            case Constant.SMS_CANCEL_INDEX:
            	stringBuilder
        		.append(Constant.SMS_CANCEL);
            	break;
        		
        	}
        	sendData = stringBuilder.toString();
			try {
			SMSSender smsSender = new SMSSender(getApplicationContext());
			smsSender.sendSMS("01051498848", sendData);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	Toast.makeText(getApplicationContext(), sendData, Toast.LENGTH_SHORT).show();
        	Log.d(TAG, "position : " + position + "...sendData : " + sendData);
            return true;
            
        }
        return super.onOptionsItemSelected(item);
    }
    
    private int validNumber(String number, String type){
    	try {
    		Integer.parseInt(number);
		} catch (Exception e) {
			Toast.makeText(this, type + "번호의 공백이나 문자 없이 숫자만 입력해 주세요.", Toast.LENGTH_SHORT).show();
			return Constant.WORNG_NUMBERFORMAT;
		}
    	
    	if(0 < number.length() &&  number.length() < 8){
    		Toast.makeText(this, type + "번호의 유효한 번호를 입력해 주세요.", Toast.LENGTH_SHORT).show();
    		return Constant.SHORT_NUM;
    	}
    		
    	
    	if(12 < number.length()){
    		Toast.makeText(this, type + "번호의 유효한 번호를 입력해 주세요.", Toast.LENGTH_SHORT).show();
    		return Constant.LONG_NUM;
    	}
    	
    	return Constant.VALIDE_NUM;
    }
    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            int sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
            
            switch(sectionNumber){

	            case Constant.SMS_HOT_LINE_NUM_INDEX:
	            	rootView = inflater.inflate(R.layout.fragment_numsetting, container, false);	
	            	break;
	        	
	            case Constant.SMS_SYSTEM_MODE_SETTING_INDEX:
	            	rootView = inflater.inflate(R.layout.fragment_systemmodesetting, container, false);
	            	RadioGroup systemModeRadiogroup = (RadioGroup) rootView.findViewById(R.id.system_mode_radiogroup);
	            	systemModeRadiogroup.setOnCheckedChangeListener(onCheckedChangeListener);
	            	break;
            }
            position = sectionNumber;
            getrootView = rootView;
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            int number = getArguments().getInt(ARG_SECTION_NUMBER);
            String title = getString(R.string.title_section);
            switch (number) {
        	case 1:
        		title = getString(R.string.title_section);
            break;
            case 2:
            	title = getString(R.string.title_section1);
                break;
            case 3:
            	title = getString(R.string.title_section2);
                break;
            case 4:
            	title = getString(R.string.title_section3);
                break;
            case 5:
            	title = getString(R.string.title_section4);
                break;
            case 6:
            	title = getString(R.string.title_section5);
                break;
            case 7:
            	title = getString(R.string.title_section6);
                break;
            case 8:
            	title = getString(R.string.title_section7);
                break;
            case 9:
            	title = getString(R.string.title_section8);
                break;
            case 10:
            	title = getString(R.string.title_section9);
                break;
            case 11:
            	title = getString(R.string.title_section10);
                break;
            case 12:
            	title = getString(R.string.title_section11);
                break;
            case 13:
            	title = getString(R.string.title_section12);
                break;
            case 14:
            	title = getString(R.string.title_section13);
                break;
            case 15:
            	title = getString(R.string.title_section14);
                break;
        }
           ((MainActivity)activity).setTitle(title);
		}
        
        static OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
    		
    		@Override
    		public void onCheckedChanged(RadioGroup group, int checkedId) {
    			// TODO Auto-generated method stub
    			switch(checkedId){
    			case R.id.elder:
    				Log.d(TAG, "Constant.SYSTEM_MODE_ELDER");
    				systemMode = Constant.SYSTEM_MODE_ELDER;
    				break;
    				
    			case R.id.disabled:
    				Log.d(TAG, "Constant.SYSTEM_MODE_DISABLED");
    				systemMode = Constant.SYSTEM_MODE_DISABLED;
    				break;
    				
    			case R.id.medical_it:
    				Log.d(TAG, "Constant.SYSTEM_MODE_MEDICAL_IT");
    				systemMode = Constant.SYSTEM_MODE_MEDICAL_IT;
    				break;
    				
    			case R.id.one_reinstalling:
    				Log.d(TAG, "Constant.SYSTEM_MODE_ONE_REINSTALLING");
    				systemMode = Constant.SYSTEM_MODE_ONE_REINSTALLING;
    				break;
    			}
    		}
    	};

	}

}
