package com.smartcaresystemsms;

/**
 * @author Jong
 *
 */
public interface Constant {
	static final String ENROLLMENT_PATH = "http://222.122.128.45:8080/admin/menu/login/ajax/user/reg";
	
	final public static int SYSTEM_MODE_ELDER = 1;
	final public static int SYSTEM_MODE_DISABLED = 2;
	final public static int SYSTEM_MODE_MEDICAL_IT = 3;
	final public static int SYSTEM_MODE_ONE_REINSTALLING = 4;
	final public static int SYSTEM_MODE_DEFAULT = SYSTEM_MODE_ELDER;
	
	
	static final int GANGWONDO_INDEX = 0;
	static final int GYEONGGIDO_INDEX = 1;
	static final int GYEONGSANGNAMDO_INDEX = 2;
	static final int GYEONGSANGBUKDO_INDEX = 3;
	static final int GWANGJU_INDEX = 4;
	static final int DAEGU_INDEX = 5;
	static final int DAEJEON_INDEX = 6;
	static final int BUSAN_INDEX = 7;
	static final int SEOUL_INDEX = 8;
	static final int SEJONG_INDEX = 9;
	static final int ULSAN_INDEX = 10;
	static final int INCHEON_INDEX = 11;
	static final int JEOLLANAMDO_INDEX = 12;
	static final int JEOLLABUKDO_INDEX = 13;
	static final int JEJU_INDEX = 14;
	static final int CHUNGCHEONGNAMDO_INDEX = 15;
	static final int CHUNGCHEONGBUKDO_INDEX = 16;
	
	
	static final int SMS_INFOMATION_INDEX = 1;
	/**
	 * 전화번호설정
	 */
	static final String SMS_HOT_LINE_NUM = "UCF:0";
	static final int SMS_HOT_LINE_NUM_INDEX = 3;
	/**
	 * 역점검요청
	 */
	static final String SMS_RESULT_CHECK = "UCF:3";
	static final int SMS_RESULT_CHECK_INDEX = 2;
	/**
	 * 원격개통
	 */
	static final String SMS_REMOTE_OPEN = "TF_01";
	static final int SMS_REMOTE_OPEN_INDEX = 4;
	/**
	 * 시스템모드셋팅
	 * 
	 */
	static final String SMS_SYSTEM_MODE_SETTING = "TF_02";
	static final int SMS_SYSTEM_MODE_SETTING_INDEX = 5;
	/**
	 * 스마트폰업데이트
	 */
	static final String SMS_GATEWAY_FIRMWARE_UPDATE = "TF_03";
	static final int SMS_GATEWAY_FIRMWARE_UPDATE_INDEX = 6;
	/**
	 * 본체업데이트
	 */
	static final String SMS_ZIGBEE_FIRMWARE_UPDATE = "TF_04";
	static final int SMS_ZIGBEE_FIRMWARE_UPDATE_INDEX = 7;
	/**
	 * 게이트웨이상태
	 */
	static final String SMS_GATEWAY_INFORMATION = "TF_05";
	static final int SMS_GATEWAY_INFORMATION_INDEX = 8;
	/**
	 * 센서상태
	 */
	static final String SMS_SENSOR_INFORMATION = "TF_06";
	static final int SMS_SENSOR_INFORMATION_INDEX = 9;
	/**
	 * 재실모드
	 */
	static final String SMS_IN_MODE_SETTING = "TF_07";
	static final int SMS_IN_MODE_SETTING_INDEX = 10;
	/**
	 * 전원복구
	 */
	static final String SMS_POWER_RECOVERY = "TF_08";
	static final int SMS_POWER_RECOVERY_INDEX = 11;
	/**
	 * 활동감지
	 */
	static final String SMS_ACTIVITY = "TF_09";
	static final int SMS_ACTIVITY_INDEX = 12;
	/**
	 * 데이터미수신
	 */
	static final String SMS_DATA_NOT_RECEIVE = "TF_10";
	static final int SMS_DATA_NOT_RECEIVE_INDEX = 13;
	/**
	 * 지역 설정
	 */
	static final String SMS_NOT_MATCH_AREA = "TF_11";
	static final int SMS_NOT_MATCH_AREA_INDEX = 14;
	/**
	 * 주기보고
	 */
	static final String SMS_PERIOD_REPORT = "TF_12";
	static final int SMS_PERIOD_REPORT_INDEX = 15;
	/**
	 * 취소
	 */
	static final String SMS_CANCEL = "TF_13";
	static final int SMS_CANCEL_INDEX = 16;
	
	static final int WORNG_NUMBERFORMAT = 0;
	static final int SHORT_NUM = 1;
	static final int LONG_NUM = 2;
	static final int VALIDE_NUM = 3;
	
	
	
}
